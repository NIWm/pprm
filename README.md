### PPRM Project

**PHÂN TÍCH THIẾT KẾ PROJECT QUẢN LÝ QUAN HỆ**
-----------------------------------------

1. GIỚI THIỆU
# Phần giới thiệu của tài liệu kiến trúc hệ thống cần cung cấp một cách tổng quan về tài liệu này bao gồm mục đích của tài liệu, các khái niệm, thuật ngữ và các tài liệu tham khảo liên quan.

- Mục đích
    - Tài liệu này cung cấp bức tranh toàn cảnh về hệ thống quản lý quan hệ
- Phạm vi
    - Tài liệu này cung cấp bức tranh toàn cảnh về hệ thống quản lý quan hệ
- Tài liệu tham khảo
- Mô tả tài liệu 
    - Tài liệu này cung cấp bức tranh toàn cảnh về hệ thống quản lý quan hệ
2. TỔNG QUAN GIẢI PHÁP 

![Project diagram!](./Diagram.jpg)

3. CORE API SERVICE
- Chức năng đăng nhập, đăng xuất, đổi mật khẩu, quên mật khẩu
    - Thiết kế chức năng đăng nhập, đăng xuất dựa trên jwt

    - Tạo chức năng đổi mật khẩu, quên mật khẩu gửi về mail (số điện thoại)
- Quản lý đối tác (cá nhân và tổ chức)
    - Tạo giao diện để tìm kiếm đối tác

    - Thiết kế chức năng cơ bản CRUD

- Quảy lý nhật ký
    - Thiết kế chức năng cơ bản CRUD

    - Lưu lịch sử thao tác của user
- Quản lý danh mục
    - Optional danh mục
- Cấu hình thông tin cảnh báo
    - Tùy chỉnh cài đặt:
        - Thông báo cho đối tác

        - Thông báo cho gia đình
- Đặt hẹn lịch
    - Thiết kế chức năng cơ bản CRUD

    - Lưu lịch sử hẹn lịch
------------------------------------
### THIẾT KẾ CƠ SỞ DỮ LIỆU

1. User Table

| Name   | Type   |
| ------ | ------ |
| Id | uuid |
| UserName   | varchar(255)   |
| Name   | nvarchar(255)   |
| PhoneNumber   | varchar(255)   |
| Email | varchar(255) |
| AvatarUrl  | varchar(255)   |
| Type   | varchar(255)   |
| Password   | varchar(255)   |
| PasswordSalt   | varchar(255)   |
| BirthDate | DateTime |
| CreatedOnDate | DateTime |
| LastModifiedOnDate | DateTime |
| CreatedByUserName | varchar(255) |
| LastModifiedByUserName | varchar(255) |
| LastActivityDate | DateTime |
| IsLockedOut | Bit |
| Locked | Bit |
| BankName | nvarchar(255) |
| BankAccountNo | varchar(255) |
| ZaloName | varchar(255) |
| UserAddressId | uuid |

2. User Address Table

| Name   | Type   |
| ------ | ------ |
| Id | uuid |
| ApartmentNumber  | varchar(255)   |
| FullTextAdress   | nvarchar(255)  |

3. RelationShip Table

| Name   | Type   | 
| ------ | ------ |
| Id | uuid |
| Code | varchar(50) |
| Name  | nvarchar(255)   |

4. User RelationShip Table

| Name   | Type   | 
| ------ | ------ |
| Id | uuid |
| Code | varchar(50) |
| Name  | nvarchar(255)   |
| UserId  | uuid   |

5. AppointMent Table

| Name   | Type   | 
| ------ | ------ |
| Id | uuid |
| CreatedOnDate | DateTime |
| LastModifiedOnDate | DateTime |
| CreatedByUserName | varchar(255) |
| LastModifiedByUserName | varchar(255) |

6. History Table

| Name   | Type   | 
| ------ | ------ |
| Id | uuid |
| Name | nvarchar(255) |
| Type | varchar(255) |
| Note | nvarchar(max) |
| CreatedOnDate | DateTime |
| CreatedByUserName | varchar(255) |

7. Reminder Table

| Name   | Type   | 
| ------ | ------ |
| Id | uuid |
| Name | nvarchar(500) |
| ReminderTime | DateTime |
| IsDaily | Bit |
| CreatedOnDate | DateTime |
| LastModifiedOnDate | DateTime |
| CreatedByUserName | varchar(255) |
| LastModifiedByUserName | varchar(255) |
